import React, {useContext, useState} from "react"
import axios from "axios";
import {MovieContext} from "./MovieContext"

const MovieFrom = () =>{
    const { value1, value2,value3,value4,value5,value6,value7,value8,value9 } = React.useContext(MovieContext);  
    const [daftarfilm, setdaftarfilm] = value1
    const [title, settitle]  =  value2
      const [description, setdescription]  =  value3
      const [year, setyear]  =  value4
      const [duration, setduration]  =  value5
      const [genre, setgenre]  =  value6
      const [rating, setrating]  =  value7
    const [indexOfForm, setIndexOfForm] =  value8
    const [id, setid] =  value9


      const handleInputForm = (event) =>{
        console.log(event.target.name);

        if(event.target.name === 'title'){
            settitle(event.target.value)
        }

        if(event.target.name === 'description'){
            setdescription(event.target.value)
        }

        if(event.target.name === 'year'){
            setyear(event.target.value)
        }

        if(event.target.name === 'duration'){
            setduration(event.target.value)
        }

        if(event.target.name === 'genre'){
            setgenre(event.target.value)
        }

        if(event.target.name === 'rating'){
            setrating(event.target.value)
        }

      }
      

      

    

    const handleSubmit = (event) =>{
        event.preventDefault()
        let newdaftarfilm = daftarfilm
        if(indexOfForm  === -1){
            newdaftarfilm = [...newdaftarfilm, {id:indexOfForm,
                                                title:title,
                                                description:description,
                                                year:year,
                                                duration:duration,
                                                genre:genre,
                                                rating:rating
                                                }]
            axios.post('http://backendexample.sanbercloud.com/api/movies', {title:title,
                                                                            description:description,
                                                                            year:year,
                                                                            duration:duration,
                                                                            genre:genre,
                                                                            rating:rating
                                                                            })
            .then(res => {
                console.log(res);
            })
        }else{
            console.log(id)
            axios.post('http://backendexample.sanbercloud.com/api/movies', {id:id,title:title,
                                                                            description:description,
                                                                            year:year,
                                                                            duration:duration,
                                                                            genre:genre,
                                                                            rating:rating})
            .then(res => {
                console.log(res);
            })
            newdaftarfilm[indexOfForm] =  {id:indexOfForm,title:title,
                                            description:description,
                                            year:year,
                                            duration:duration,
                                            genre:genre,
                                            rating:rating}
            newdaftarfilm = newdaftarfilm
        }

        
      

        setdaftarfilm(newdaftarfilm)
        settitle("")
        setdescription("")
        setyear("")
        setduration("")
        setgenre("")
        setrating("")
      }

  return(
    <div>
      <h1>Form Movie</h1>
        <form onSubmit={handleSubmit}>
          <div>
          <label className="paddingright">
            Masukkan nama Film:
          </label>          
          <input type="text" name="title" value={title} onChange={handleInputForm}/>
          </div>
          <div>
          <label className="paddingright">
            Masukkan Description Film: 
          </label> 
           <input type="text" name="description" value={description} onChange={handleInputForm}/>
          </div>
          <div>
          <label className="paddingright">
            Masukkan Tahun Film:
          </label> 
          <input type="number" placeholder="YYYY" min="1945" max="2100" format name="year"  value={year} onChange={handleInputForm}/>
          </div>

          <div>
          <label className="paddingright">
            Masukkan Durasi Film:
          </label> 
          <input type="number" name="duration"  value={duration} onChange={handleInputForm}/>
          </div>

          <div>
          <label className="paddingright">
            Masukkan Genre Film:
          </label> 
          <input type="text" name="genre"  value={genre} onChange={handleInputForm}/>
          </div>

          <div>
          <label className="paddingright">
            Masukkan Rating Film:
          </label> 
          <input type="number" min="0" max="10"  name="rating"  value={rating} onChange={handleInputForm}/>
          </div>
          <button>submit</button>
        </form>
    </div>
  )

}

export default MovieFrom