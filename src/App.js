import React, { Component,useState  } from 'react';

import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Home from './Home';
import About from './About';
import Movie from './Movie';
import Login from "./Login";

import { AuthContext } from "./context/auth";
import PrivateRoute from './PrivateRoute';


function App(props) {
  const existingTokens = JSON.parse(localStorage.getItem("tokens"));
  const [authTokens, setAuthTokens] = useState(existingTokens);
  
  const setTokens = (data) => {
    localStorage.setItem("tokens", JSON.stringify(data));
    setAuthTokens(data);
  }

  return (
    <AuthContext.Provider value={{ authTokens, setAuthTokens: setTokens }}>
      <Router>
        <header>
        <img id="logo" src="../../images/logo.png" width="200px" />
        <nav>
                <ul>
                <li><Link to="/">Home</Link></li>
                <li><Link to="/about">About</Link></li>
                <li><a href="/movie">Movie List Editor </a> </li>
                </ul>
            
            </nav>
          
        </header>
        
            <Switch>
              <Route path="/about">
                <About_ />
              </Route>
              <Route path="/movie" >  
              <Movie_ />
              </Route>
              <Route path="/login">
              <Login_ />
              </Route>
              <Route path="/">
                <Home />
              </Route>
            </Switch>
  
        <footer>
          <h5>copyright &copy; 2020 by Sanbercode</h5>
        </footer>;
        </Router>
        </AuthContext.Provider>
  );
}


function Login_() {
  return <Login />;
}

function About_() {
  return <About />;
}

function Movie_() {
  return <PrivateRoute path="/movie" component={Movie} />
}

export default App;
