import React,{ useState } from "react";
import { Link, Redirect  } from 'react-router-dom';
import { useAuth } from "./context/auth";


function Login() {

    const [isLoggedIn, setLoggedIn] = useState(false);
    const [isError, setIsError] = useState(false);
    const [userName, setUserName] = useState("");
    const [password, setPassword] = useState("");
    const { setAuthTokens } = useAuth();

    function postLogin() {
        
        console.log(userName);
        console.log(password);

        setAuthTokens('asdasd');
        setLoggedIn(true);
    }
    
    if (isLoggedIn) {
        return <Redirect to="/movie" />;
      }
  return (
      
    <section>

      <form>
        <input
          type="username"
          value={userName}
          onChange={e => {
            setUserName(e.target.value);
          }}
          placeholder="email"
        />
        <input
          type="password"
          value={password}
          onChange={e => {
            setPassword(e.target.value);
          }}
          placeholder="password"
        />
        <button onClick={postLogin}>Sign In</button>
      </form>
    </section>
  );
}

export default Login;