import React from "react"
import {MovieProvider} from "./MovieContext"
import MovieList from "./Movielist"
import MovieForm from "./MovieForm"
import { useAuth } from "./context/auth";

const Movie = () =>{

    const { setAuthTokens } = useAuth();

  return(
    <MovieProvider>
      <MovieList/>
      <MovieForm/>
    </MovieProvider>
  )
}

export default Movie