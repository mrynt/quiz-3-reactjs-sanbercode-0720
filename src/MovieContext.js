import React, { useState, createContext,useEffect } from "react";
import axios from "axios";
export const MovieContext = createContext();

export const MovieProvider = props => {
    const [daftarfilm, setdaftarfilm] =  useState(null)
    const [titles, settitle]  =  useState("")
    const [descriptions, setdescription]  =  useState("")
    const [years, setyear]  =  useState("")
    const [durations, setduration]  =  useState("")
    const [genres, setgenre]  =  useState("")
    const [ratings, setrating]  =  useState("")
    const [indexOfForm, setIndexOfForm] =  useState(-1)
    const [id, setid] =  useState(-1)  


    useEffect( () => {
        if(daftarfilm ===null){
            axios.get('http://backendexample.sanbercloud.com/api/movies')
            .then(res => {
        
                setdaftarfilm(res.data.map(el => {return {id:el.id,
                                                     title:el.title,
                                                     rating:el.rating,
                                                     genre:el.genre,
                                                     duration:el.duration,
                                                     description:el.description,
                                                     year:el.year    
                                                    }}))
            })
        }
       
      })

  return (
    <MovieContext.Provider value={{value1:[daftarfilm, setdaftarfilm],
      value2:[titles, settitle],
      value3 :[descriptions, setdescription],
      value4 :[years, setyear],
      value5 :[durations, setduration],
      value6 :[genres, setgenre],
      value7 :[ratings, setrating],
      value8:[indexOfForm, setIndexOfForm],
      value9:[id, setid]
                                            }}>
      {props.children}
    </MovieContext.Provider>
  );
};