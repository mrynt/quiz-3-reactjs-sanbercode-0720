import React, {useContext,useState} from "react"
import axios from "axios";
import {MovieContext} from "./MovieContext"

const Movielist = () =>{
    const { value1, value2,value3,value4,value5,value6,value7,value8,value9 } = React.useContext(MovieContext);  
  const [daftarfilm, setdaftarfilm] = value1
  const [title, settitle]  =  value2
    const [description, setdescription]  =  value3
    const [year, setyear]  =  value4
    const [duration, setduration]  =  value5
    const [genre, setgenre]  =  value6
    const [rating, setrating]  =  value7
  const [indexOfForm, setIndexOfForm] =  value8
  const [id, setid] =  value9

  const handleEdit = (event) =>{
    let index = event.target.value;
    let indexarr = event.target.getAttribute('data-indexarr')
    axios.put('http://backendexample.sanbercloud.com/api/movies/'+index)
    .then(res => {
        settitle(res.data.title)
        setdescription(res.data.description)
        setyear(res.data.year)
        setduration(res.data.duration)
        setgenre(res.data.genre)
        setrating(res.data.rating)
        setIndexOfForm(indexarr)
        setid(res.data.id)
    })

    
  }



const handleDelete = (event) =>{
    let index = event.target.value;
    let indexarr = event.target.getAttribute('data-indexarr')
    let newdaftarfilm = daftarfilm

    axios.delete('http://backendexample.sanbercloud.com/api/movies/'+index)
    .then(res => {
        newdaftarfilm.splice(indexarr,1)
        setdaftarfilm([...newdaftarfilm])
    })
    
  } 
    
  return(
      <div>
        <h2><b>Tabel Movie List</b></h2>
            <table className="center">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>title</th>
                        <th>description</th>
                        <th>year</th>
                        <th>duration</th>
                        <th>genre</th>
                        <th>rating</th>
                        
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                {daftarfilm !== null && daftarfilm.map((val,index)=> {
                    return (
                        <tr key={index}> 
                            <td>{index+1}</td>
                            <td>{val.title}</td>
                            <td>{val.description}</td>
                            <td>{val.year}</td>
                            <td>{val.duration}</td>
                            <td>{val.genre}</td>
                            <td>{val.rating}</td>
                            <td>
                                <button onClick={handleEdit} value={val.id} data-indexarr={index}>Edit</button>
                                &nbsp;
                                <button onClick={handleDelete} value={val.id} data-indexarr={index}>Delete</button>
                            </td>
                        </tr>
                    )
                    })}
            
            </tbody>
        </table>
      </div>
  )

}

export default Movielist