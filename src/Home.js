import React, {useState,useEffect} from 'react';
import axios from "axios";
const Component = () => {

    const [daftarfilm, setdaftarfilm] =  useState(null)
    
    useEffect( () => {
        if(daftarfilm ===null){
            axios.get('http://backendexample.sanbercloud.com/api/movies')
            .then(res => {
                const item = res.data;
                item.sort(function (a, b) {
                    return  b.rating - a.rating;
                  });

                setdaftarfilm(item.map(el => {return {id:el.id,title:el.title,rating:el.rating,genre:el.genre,duration:el.duration,description:el.description}}))
            })
        }
       
      })



    return (
        <section>
        <h1>Daftar Film Terbaik</h1>
        <div id="article-list">
        {daftarfilm !== null && daftarfilm.map((val,index)=> {
            return (
                
                    <div>
                    <a href=""><h3>{val.title}</h3></a>
                    <br />
                    
                    <p><strong>Rating {val.rating}</strong></p>
                    <p><strong>Durasi: {val.duration}</strong></p>
                    <p><strong>genre: {val.genre}</strong></p>
                    <br />
                    <p>
                    <strong>deskripsi :</strong> {val.description}
                    </p>
                    </div>
                
            )
            })}
           </div>
      </section>
    )

}


  export default Component;